package com.cy.threadt.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.core.task.TaskDecorator;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ThreadPoolExecutor;

@Slf4j
@EnableAsync
@Configuration
public class ThreadPoolConfig {

    @Bean
    public AsyncTaskExecutor asyncTaskExecutor() {
        log.info("init asyncTaskExecutor");
        int coreSize = Runtime.getRuntime().availableProcessors() * 2;
        int maximumPoolSize = coreSize * 2;
        int keepAliveTime = 10;
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(coreSize);
        executor.setMaxPoolSize(maximumPoolSize);
        executor.setQueueCapacity(100000);
        executor.setKeepAliveSeconds(keepAliveTime);
        executor.setThreadNamePrefix("async-");
//        executor.setTaskDecorator(new CustomTaskDecorator());
        executor.setRejectedExecutionHandler( new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        log.info("asyncServiceExecutor params----corePoolSize:{},maxPoolSize:{},keepAliveSeconds:{}" ,
                coreSize,maximumPoolSize,keepAliveTime);
        return executor;
    }

    /**
     * 线程池修饰类
     */
    class CustomTaskDecorator implements TaskDecorator {
        @Override
        public Runnable decorate(Runnable runnable) {
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            AsyncContext asyncContext = null;
            if (attributes!=null) {
                HttpServletRequest request = attributes.getRequest();
                asyncContext = request.startAsync();
            }
            AsyncContext finalAsyncContext = asyncContext;
            return () -> {
                try {
                    RequestContextHolder.setRequestAttributes(attributes,true);
                    runnable.run();
                } finally {
                    RequestContextHolder.resetRequestAttributes();
                    if (finalAsyncContext !=null) {
                        finalAsyncContext.complete();
                    }
                }
            };
        }
    }
}

package com.cy.threadt;

import com.cy.threadt.entity.MyResponse;
import com.cy.threadt.service.MyService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
@SpringBootTest
class ThreadTApplicationTests {

	@Resource
	private MyService myService;

	private final int count = 10000;
	private final CountDownLatch countDownLatch = new CountDownLatch(count);
	@Test
	void contextLoads() throws InterruptedException {
		for (int i = 1; i <= count; i++) {
			int finalI = i;
			new Thread( () -> {
				try {
					countDownLatch.countDown();
					countDownLatch.await();
					MyResponse query = myService.query(String.valueOf(finalI));
//					log.info("查询{},结果{}", finalI , query);
				} catch (InterruptedException | ExecutionException e) {
					throw new RuntimeException(e);
				}
			}).start();
		}

		TimeUnit.SECONDS.sleep(5);
	}

}

package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = "com.cy")
public class ThreadTApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThreadTApplication.class, args);
	}

}

package com.cy.threadt.controller;

import com.cy.threadt.entity.MyResponse;
import com.cy.threadt.service.MyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/cy/")
public class MyController {
    @Resource
    private MyService myService;

    @GetMapping(value = "/get/{orderId}")
    public Callable<MyResponse> query(@PathVariable String orderId){
        Callable<MyResponse> callable = new Callable() {
            @Override
            public Object call() throws Exception {
                return myService.query(orderId);
            }
        };
        return callable;
    }
}

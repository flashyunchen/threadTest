package com.cy.threadt.service;

import com.cy.threadt.entity.MyRequest;
import com.cy.threadt.entity.MyResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MyService {
    private final LinkedBlockingDeque<MyRequest> linkedBlockingDeque = new LinkedBlockingDeque<>(100000);

    @PostConstruct
    public void doBiz () {
        ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(1);
        threadPool.scheduleAtFixedRate(() -> {
            if (linkedBlockingDeque.size() == 0) {
                return;
            }
            List<MyRequest> requests = new ArrayList<>();
            for (int i = 0; i < linkedBlockingDeque.size(); i++) {
                requests.add(linkedBlockingDeque.poll());
            }
            batchQuery(requests);
            log.info("批查询处理数量{}", requests.size());
        }, 100, 50, TimeUnit.MILLISECONDS);
    }
    public MyResponse query(String orderId) throws ExecutionException, InterruptedException {
        MyRequest request = new MyRequest();
        request.setOrderId(orderId);
        request.setId(UUID.randomUUID().toString());
        CompletableFuture<MyResponse> objectCompletableFuture = new CompletableFuture<>();
        request.setFuture(objectCompletableFuture);
        linkedBlockingDeque.add(request);
        return objectCompletableFuture.get();
    }

    public void batchQuery(List<MyRequest> list){
        Map<String, List<MyRequest>> mapRequest = list.stream().collect(Collectors.groupingBy(MyRequest::getOrderId));
        List<String> orderIds = list.stream().map(MyRequest::getOrderId).distinct().collect(Collectors.toList());
        for (String orderId : orderIds) {
            List<MyRequest> myRequests = mapRequest.get(orderId);
            BigDecimal money = BigDecimal.valueOf(Math.random());
            for (MyRequest myRequest : myRequests) {
                MyResponse response = new MyResponse();
                response.setOrderId(orderId);
                response.setMoney(money);
                myRequest.getFuture().complete(response);
            }
        }
    }
}
